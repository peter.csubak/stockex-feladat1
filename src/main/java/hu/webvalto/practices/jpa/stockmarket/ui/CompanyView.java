package hu.webvalto.practices.jpa.stockmarket.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.CssLayout;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import org.vaadin.cdiviewmenu.ViewMenuItem;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@UIScoped
@CDIView("Company")
@ViewMenuItem(order = 5, icon = FontAwesome.ARROWS_ALT, title = "Companies")
public class CompanyView extends CssLayout implements View {

    @Inject
    private BaseDao baseDao;

    private MTable<Company> companyTable = new MTable<>(Company.class)
            .withFullWidth()
            .withHeight("450px")
            .withProperties("name",
                    "details")
            .withColumnHeaders("Name", "Details");

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        refreshTable();
    }

    @PostConstruct
    private void init() {
        addComponents(
                new MVerticalLayout(
                        new MHorizontalLayout(companyTable).withFullWidth()
                ).withSize(MSize.FULL_SIZE)
        );
        setSizeFull();
        refreshTable();
    }

    private void refreshTable() {
        companyTable.setBeans(baseDao.findAll(Company.class));
    }
}
