package hu.webvalto.practices.jpa.stockmarket.daos;

import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.entities.PortfolioElement;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Named
public class TradeDaoImpl implements TradeDao {

    @Inject
    private BaseDao baseDao;

    @PersistenceContext(unitName = "stockPU")
    private EntityManager entityManager;

    @Override
    public List<Stock> findStocksForBuying(Broker broker) {
        return baseDao.findAll(Stock.class);
    }

    @Override
    public List<Stock> findStocksForSelling(Broker broker) {
        return Collections.emptyList();
    }

    @Override
    public List<Trade> findExpiringTrades() {
        return Collections.emptyList();
    }

    @Override
    public List<Trade> findSellingTrades() {
        return Collections.emptyList();
    }

    @Override
    public List<Trade> findBuyingTradesByPriceAndVolumeLimit(double priceLimit, long volumeLimit, Company company) {
        return Collections.emptyList();
    }
}
