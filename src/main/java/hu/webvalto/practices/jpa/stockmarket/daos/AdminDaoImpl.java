package hu.webvalto.practices.jpa.stockmarket.daos;

import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.entities.PortfolioElement;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.UUID;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Named
@Transactional
public class AdminDaoImpl implements AdminDao {

    @PersistenceContext(unitName = "stockPU")
    private EntityManager entityManager;


    @Override
    public void initialize() {
        createStock();
    }

    private void createStock() {
        Company company1 = Company.builder().id(UUID.randomUUID().toString()).name("company1").details(getLorem()).build();
        Broker broker1 = Broker.builder().id(UUID.randomUUID().toString()).name("broker1").company(company1).build();
        entityManager.persist(broker1);
        Company company2 = Company.builder().id(UUID.randomUUID().toString()).name("company2").details(getLorem()).build();
        Broker broker2 = Broker.builder().id(UUID.randomUUID().toString()).name("broker2").company(company2).build();
        entityManager.persist(broker2);
        Stock stock1 = Stock.builder().id(UUID.randomUUID().toString()).name("stock1").symbol("sym1").price(100d).build();
        entityManager.persist(stock1);
        Stock stock2 = Stock.builder().id(UUID.randomUUID().toString()).name("stock2").symbol("sym2").price(100d).build();
        entityManager.persist(stock1);
        PortfolioElement portfolioElement1 = PortfolioElement.builder().id(UUID.randomUUID().toString()).volume(10L).company(company1).stock(stock1).build();
        entityManager.persist(portfolioElement1);
        PortfolioElement portfolioElement2 = PortfolioElement.builder().id(UUID.randomUUID().toString()).volume(10L).company(company2).stock(stock2).build();
        entityManager.persist(portfolioElement2);
    }


    @Override
    public void clear() {
        entityManager.createQuery("delete from Trade").executeUpdate();
        entityManager.createQuery("delete from Broker").executeUpdate();
        entityManager.createQuery("delete from PortfolioElement").executeUpdate();
        entityManager.createQuery("delete from Stock").executeUpdate();
        entityManager.createQuery("delete from Company").executeUpdate();
    }

    private String getLorem() {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique egestas tellus et accumsan. In hac habitasse platea dictumst. Donec ornare vitae tellus sit amet sodales. Fusce elementum mauris enim, vitae lacinia tellus euismod ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac erat libero. Nulla vel pretium sapien. Etiam ac elit in orci pulvinar suscipit ut sed magna. Donec vel suscipit sapien. Integer nec ullamcorper justo. Vivamus posuere diam quis convallis pretium. Nullam quis elementum dolor, eget facilisis eros. Integer et pharetra lorem. Integer dictum tellus ut leo vehicula venenatis. Maecenas ut congue ex, sed semper mi.\n" +
                "\n" +
                "Nunc pellentesque ligula id semper consectetur. Vestibulum non nulla sapien. Aliquam lobortis imperdiet velit, nec luctus nisl varius vitae. Proin placerat luctus nulla, quis auctor diam feugiat vitae. In aliquam pretium sapien id auctor. Proin lorem sem, rutrum vitae nisl cursus, ultrices fermentum sapien. Phasellus eget urna euismod, sollicitudin quam ut, porttitor quam. Mauris luctus interdum turpis, nec vehicula velit mattis nec. Phasellus vel euismod sem, ut dapibus felis. Morbi blandit, dui in placerat malesuada, velit sem ornare turpis, ac tincidunt orci ligula nec odio.\n" +
                "\n" +
                "Pellentesque id augue turpis. Integer tincidunt libero mi, eget faucibus orci facilisis eget. Ut cursus ipsum vitae nulla bibendum accumsan. Nunc fringilla libero quis pretium gravida. Donec a ante erat. Suspendisse sit amet mauris ligula. Integer quis risus nulla.\n" +
                "\n" +
                "Praesent venenatis vel diam non sollicitudin. Nulla ac maximus orci, sit amet facilisis dolor. Vestibulum ligula lacus, tempus eget dictum sed, finibus ut est. Fusce eget arcu a ligula cursus tincidunt. Aliquam quis dignissim dolor. Sed semper euismod magna ac mollis. Proin ultricies ac nibh et consectetur. In posuere eu mi et bibendum. Suspendisse lacinia est diam. Nulla suscipit orci diam, id lobortis mi accumsan ac. Aliquam ut malesuada urna, vel bibendum massa. In consequat sapien et vulputate congue. Suspendisse volutpat pharetra nisi, at accumsan eros condimentum et. Donec nec gravida sapien.\n" +
                "\n" +
                "Fusce at urna vitae urna rhoncus venenatis vel ac nibh. Fusce id ligula sit amet nisi aliquet facilisis. Morbi eleifend metus at libero porttitor, ac commodo ligula imperdiet. Nulla tristique tellus massa. In hac habitasse platea dictumst. Praesent eu diam maximus ligula rhoncus hendrerit sed at sapien. Nam porttitor bibendum sodales. Integer finibus, erat non dictum consequat, enim orci rhoncus quam, non semper erat sem placerat ex. Ut vestibulum augue dui, ac fringilla erat tempor in. Phasellus egestas ut orci ac sollicitudin.\n" +
                "\n" +
                "In efficitur pharetra arcu, eget porta turpis interdum ac. Integer ut metus id orci cursus iaculis vitae eu dui. Duis vel auctor augue. Vestibulum lacinia orci dolor, eget pretium neque gravida nec. Vestibulum condimentum tellus id ex malesuada, eget aliquam tortor sagittis. Nullam aliquet vel nisi non malesuada. Aenean eu rhoncus turpis. Nam nec quam diam. In hac habitasse platea dictumst.\n" +
                "\n" +
                "Pellentesque suscipit rhoncus condimentum. Praesent a ex sed mi rutrum lobortis. Maecenas pellentesque rhoncus tortor nec dictum. Nullam eleifend aliquet est sed maximus. Donec eget sollicitudin eros, sed pellentesque nisl. Praesent quis risus sed lorem finibus tincidunt. Praesent lobortis facilisis massa eget cursus. Aliquam sed mauris vulputate, aliquet lectus non, volutpat purus. Curabitur dictum hendrerit velit nec egestas. Proin eget tortor sed turpis congue iaculis. Suspendisse quis purus nunc. Vivamus tempor, justo lacinia iaculis finibus, arcu purus auctor dolor, quis commodo enim orci id quam.\n" +
                "\n" +
                "Mauris vitae mollis diam, at bibendum enim. Vestibulum lacus lorem, varius eu fringilla quis, tempor id ipsum. Ut nisi nibh, malesuada eu lacus eu, aliquam placerat leo. Mauris dolor enim, volutpat sollicitudin sollicitudin eget, placerat quis lorem. In interdum leo id enim ullamcorper dapibus. Suspendisse lectus arcu, dictum vitae erat eu, tempor viverra massa. In in scelerisque nisl, vel volutpat ipsum. Sed pharetra elit sed sodales placerat. Suspendisse in feugiat odio. Aliquam volutpat in mi ut fermentum. Donec molestie purus elit, id sagittis ipsum iaculis ac.\n" +
                "\n" +
                "Ut placerat molestie blandit. Duis porta cursus urna, id commodo tortor rutrum ac. Aliquam et dui sed tortor feugiat blandit. Morbi molestie lobortis magna, et fermentum ligula blandit sit amet. Vestibulum gravida feugiat ligula a tincidunt. Vivamus vitae ante feugiat, gravida odio quis, dictum nisi. Phasellus ante dui, ornare quis erat ut, ullamcorper varius velit. Cras ut ex id purus dictum efficitur quis eu neque.\n" +
                "\n" +
                "Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla quis lacus eu massa ornare semper sit amet vitae enim. Sed egestas massa ex, suscipit dapibus neque sollicitudin bibendum. Morbi ut elit at augue dignissim ultrices. Aliquam lacinia dapibus est, ut mollis augue malesuada eget. Pellentesque ac mattis mi. Sed vel blandit ex, non pellentesque urna. Suspendisse mauris ante, pellentesque ac enim et, cursus placerat mauris. Aliquam dictum facilisis fringilla. Praesent egestas turpis vel neque gravida mattis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus volutpat tortor nisi, a pulvinar libero sollicitudin in. Nunc cursus odio ac faucibus ornare.";
    }
}
