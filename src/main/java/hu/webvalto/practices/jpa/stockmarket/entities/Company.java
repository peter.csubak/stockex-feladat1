package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Cacheable(value = false)
@Table(name = "COMPANIES")
public class Company {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

    private String details;

    @Transient //TODO feladat 1 módosítsd kapcsolatra (legen a fetch = FetchType.EAGER mert később számít rá a kód)
    private List<PortfolioElement> portfolio;

    public String toString() {
        return name;
    }
}
