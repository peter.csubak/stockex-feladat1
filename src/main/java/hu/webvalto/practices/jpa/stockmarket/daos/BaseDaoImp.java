package hu.webvalto.practices.jpa.stockmarket.daos;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Named
@Transactional
public class BaseDaoImp implements BaseDao {

    @PersistenceContext(unitName = "stockPU")
    private EntityManager entityManager;

    @Override
    public <T> List<T> findAll(Class<T> entityClass) {
        //TODO feladat 1 implementáld
        return Collections.emptyList();
    }

    @Override
    public <T> void save(T entity) {
        //TODO feladat 1 implementáld
    }

    @Override
    public <T> void delete(T entity) {
        //TODO feladat 1 implementáld
    }

    @Override
    public <T> void deleteAll(Class<T> entityClass) {
        //TODO feladat 1 implementáld
    }

}
