package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Table(name = "PORTFOLIO")
@Cacheable(value = false)
public class PortfolioElement {

    @Id
    private String id = UUID.randomUUID().toString();

    private Long volume;

    @Transient //TODO feladat 1 módosítsd kapcsolatra
    private Company company;

    @Transient //TODO feladat 1 módosítsd kapcsolatra
    private Stock stock;



}
