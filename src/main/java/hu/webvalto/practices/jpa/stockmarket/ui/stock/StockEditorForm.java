package hu.webvalto.practices.jpa.stockmarket.ui.stock;

import com.vaadin.ui.Component;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MMarginInfo;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
public class StockEditorForm extends AbstractForm<Stock> {

    private MTextField name = new MTextField("Name");
    private MTextField symbol = new MTextField("Symbol");
    private MTextField price = new MTextField("Price");

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                getToolbar(),
                new MHorizontalLayout(
                        new MFormLayout(
                                name,
                                symbol,
                                price
                        ).withMargin(false)
                )
        ).withMargin(new MMarginInfo(false, true));
    }
}
