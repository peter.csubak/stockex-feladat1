package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "TRADES")
public class Trade {

    @Id
    private String id = UUID.randomUUID().toString();

    @Transient //TODO feladat 1 tároljuk db-ben string formában
    private TradeType tradeType;

    private Long volume;

    private Double price;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expires;

    @Transient //TODO feladat 1 tároljuk db-be string formában
    private TradeStatusType status;

    @Transient //TODO feladat 1 módosítsd kapcsolatra
    private Stock stock;

    @Transient //TODO feladat 1 módosítsd kapcsolatra
    private Company company;

}
