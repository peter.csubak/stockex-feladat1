package hu.webvalto.practices.jpa.stockmarket.daos;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public interface AdminDao {

    void initialize();

    void clear();
}
