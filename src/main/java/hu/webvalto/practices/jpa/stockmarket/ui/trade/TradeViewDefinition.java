package hu.webvalto.practices.jpa.stockmarket.ui.trade;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import hu.webvalto.practices.jpa.stockmarket.daos.TradeDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.ui.VaadinUI;
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.fields.LazyComboBox;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public abstract class TradeViewDefinition extends CssLayout implements View {

    @Inject
    protected TradeDao tradeDaoService;

    private LazyComboBox<Stock> stockLazyComboBox;
    private TextField volumeField;
    private Button tradeButton;
    private TextField priceField;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Broker loggedInBroker = getLoggedInBroker();
        if (loggedInBroker != null && loggedInBroker.getName().toLowerCase().equals("admin")) {
            tradeButton.setEnabled(false);
        }
        stockLazyComboBox.setBeans(getStocks());
    }

    @PostConstruct
    public void init() {
        setupComboBox();
        setupVolumeField();
        setupSendButton();
        setupPrice();
        addComponent(new MVerticalLayout(
                new MFormLayout(stockLazyComboBox,
                        volumeField,
                        priceField,
                        tradeButton)
        ).withSize(MSize.FULL_SIZE));

    }

    private void setupPrice() {
        priceField = new TextField("Price:");
        priceField.setValue("1");
        priceField.setSizeFull();
    }

    private void setupSendButton() {
        tradeButton = new Button("Trade");
        tradeButton.addClickListener(event -> {
            makeTrade();
            ViewMenuUI.getMenu().navigateTo(TradesView.class);
        });
    }

    private void setupComboBox() {
        stockLazyComboBox = new LazyComboBox<>(Stock.class);
        stockLazyComboBox.setNullSelectionAllowed(false);
        stockLazyComboBox.setBeans(getStocks());
        stockLazyComboBox.setCaption("Stock: ");
        stockLazyComboBox.setSizeFull();
    }

    protected abstract List<Stock> getStocks();

    private void setupVolumeField() {
        volumeField = new TextField("Volume:");
        volumeField.setValue("1");
        volumeField.setSizeFull();
    }

    protected Stock getSelectedStock() {
        return stockLazyComboBox.getValue();
    }

    protected Long getVolumeValue() {
        return Long.valueOf(volumeField.getValue());
    }

    public Double getPriceValue() {
        return Double.valueOf(priceField.getValue());
    }

    protected abstract void makeTrade();

    protected Broker getLoggedInBroker() {
        return ((VaadinUI) VaadinUI.getCurrent()).getLoggedInBroker();
    }
}
